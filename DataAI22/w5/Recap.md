# Recap of Database Performance
Most common = **Indexes**

## B-Tree Index

Index = Data Structure in which you can quickly navigate to find the particular row.

To find that quickly, a **tree structure** is generated, this tree is divided into **ranges of possible values**. 
	→ This way the tree can be run through in a limited amount of steps to find the correct row.

**![http://docs.oracle.com/cd/E11882_01/server.112/e40540/img/cncpt244.gif](https://lh4.googleusercontent.com/t39CM4NMuEw4XGKmF-k3EU6Wha3UGTRhEtEFfi1jPCWNVa718Yyo4OvvLdkPSPllea3HVGpwhQBbugazD8Y6Nev8bOGExLtqJe8A34icJXHxsIIFrc1wUVhMaQ13PQbpO3UpHvSLmzHL)**

Many rows of table → **full table scan** (Common in DWH)

## Function-based Indices
= Index on calculated data

When filtering using a function like UPPER() or LOWER(), the b-tree index will never be used, it will always be the function based index.

## Table Spaces
using tablespaces = optimizing DB

Lesser role in last years, because more flexible architectures like public/private/hybrid clouds. a DBA (Database Admin) no longer has a clear view on the physical location of volumes.

## Execution Plan Optimazation

RDBMS does its best to retrieve data as efficiëntly as possible, still, DBA can improve with *hints*. Critical u look at queries made by an ORM (Object Relational Mapping) launches and make necessary adjustments.