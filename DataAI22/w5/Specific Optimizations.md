# Specific DWH Optimizations
## Difference SQL Server
index organized tables → similair → Table organized as b-tree based on PK.
	→ Significant performance gains with frequentle querying the DB.

Cluster Index = Table is stored physically in B-tree kind of way.

---

## Design 
Analysis questions look through a lot of rows → High cost

To lower cost, additional optimization techniques.

-  [[Bitmap]]
-  [[Materialized Views Indexed views and Query Rewrite]]
-  [[Partitioning]]
-  [[Column Storage]]
-  [[Olap Cubes]]
-  [[In-memory DB]]
-  [[Analysis Services]]
-  [[Insert Update optimizations]]
