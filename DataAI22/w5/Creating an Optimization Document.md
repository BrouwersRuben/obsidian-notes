# Creating Optimization Document
Optimization = Functional

1 form of optimazation can be good for 1 thing but not for the other...

Use **Tuning Advisor**
	Advice on how to make queries run faster

- Set options to analyse more or less optimisation possibilities
- Assess the recommendation plans and see if an optimization is good for the rest of the optimizations
- **NO END POINT, ONLY SUGGEST**

**Before** executing optimization: 
1. Run the queries you want to optimize and include an actual query plan
2. Save execution plan
3. Inspect the Subtree cost on the select.

**After** executing the optimizations:
1. run queries again
2. Compare the execution plans 
3. Compare the cost subtrees
4. analyze the rest of the plan to see where the differences are.

## Are you going to do it?
![Top Shia Labeouf Do It Stickers for Android & iOS | Gfycat](https://thumbs.gfycat.com/GratefulEdibleGrayreefshark-max-1mb.gif)

Questions: 
- Do the advantages outweigh the disadvantages?
- What are the concequences?
- Does it cause delays elsewhere?
- How big is the speed gain?
- How big is the impact on end user?
- ...

**DOCUMENT!!**