= saves a bitmap for each possible value in a field

bitmap sets all rows to 0 if the field does not contain that value and to 1 if that field does contain that particular value

Bitmaps → Get really large

Row ID's is generally a lot faster

> "number of distinct values / number of rows <= 1%" → Bitmap

Negative = require loads of resources to create. (used when limited number of updates and inserts aka *DWH*)