Normally store data by row → could also be done by column.

Because there is often duplicate data in rows, high compression can be applied to a column → Faster Queries

Especially good when = queries only need a limited amount of columns for many rows.

Usefull on [[Center table]] and larger [[Dimension table]].

Negative = IO is a bigger bottleneck