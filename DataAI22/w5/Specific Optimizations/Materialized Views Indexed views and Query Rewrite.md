**![http://www.dba-oracle.com/images/u00320020206brl09_02.gif](https://lh6.googleusercontent.com/4or0ucZWy5k3hWiiau08ATgu9_7tKIIc48Ge0kEBJNkeEKDrjH4MrRhfoiPJBnSO1QtNRGtgk_4_9UPO3DcLCxL4jF5EUyimxYcms7_XojeSjWxmGIyVd05kXH5Xl685MQkkePSxsKep)**
Materialized view (in SQLServer Indexed View) = View that is saved physically.

Query rewrite = technique to see if a query can be done in a 'Materialized View' rather than on OG table.

1. DB looks for materialized view if exists
2. DB calculates cost on MV and on OG table
3. DB picks query with lowest cost.

(Considering it being an aggregated result, the materialized table will be a lot smaller than the original sales table).

Negative = Requires loads of power to create, has to be done on OG tables with every change. 

Can be done in the background, but then the query is on OG table and less performant.