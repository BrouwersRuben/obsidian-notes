Memory = not big problem anymore → tables fully in memory

**Good** for transactional queries
**Bad** for analytical queries

(Since memory is not Durable, the data is still placed on disk behind it so that everything is recoverable.)

[[OLAP]] → Analysis Services in Tabular mode
* Data largely stored in memory
	* Combined with column storage