#### Switch of FK
when insert, every FK is **verified** in Foreign Tables

- Operational System /= Problem
- DWH = Major impact on performance

Precondition = The references are checked at application-level.

#### Insert-specific Optimizations
- Group row into 1 insert statement with several value lists.
- Make use of alternative insert techniques (bulk loading, load_data-infile)
- limit the number of transactions

#### Update-specific Optimizations
- Limit number of columns in the statement to what is strictly necessary.
	- NOT EASY IN ETL