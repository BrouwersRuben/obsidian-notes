# The Emergence of NoSQL
Start of IT project: loads of questions. (platform java? Cloud? Framework? ...)
2 constants: Object Oriented a **Relational**

## Relational DB
OLTP = normalized, SQL, ...
OLAP = denormalized (performance)

### Good
- Many types & Tools
- Lot of know-how
- SQL
- easy to integrate

#### ACID 
= Reliablity
##### Atomicity
Everything works or everything fails
!= Partial Success
##### Consistency
DB remains consistant before/after transaction
##### Isolation
Isolation Level determines degree of influence between parallel transactions on each other.
##### Durability
Result of transaction is permanent 

### Bad
- **Impendence Mismatch** with OO domain
	need tools like hibernate and stuff
	(no inheritance, no associations (FK, but not...), ...)
- Modifications to schema have a big impact
	Conversion scripts, ...
- Complex horizontal Scalability
	Most stuff is intended for Single Server Model
	Data model less suitable for replication
	license fee per node can be high
	...

### Main Players
![[Pasted image 20220508222329.png]]
--> Nothing lasts forever

## Big Data
- **Volume**
- **Velocity**
- **Variety**
- (Not Structured)
- Relaxing **A**C**I**D
	- Consistency = less crucial
	- Durabiliry = less crucial
- Sustainablity less important, only want to know the Real-Time Data
### Players
Google (personalized adds, ...); IoT (everything with an IP sends out data, free wifi analytical processing (Ikea), OS (Android, Windows 10, ...) keep a bunch of data on you and sell it)

Amount of players grows exponentially, especially with IoT data is produced in an endless stream.

DWH hits it limitations right there, a DWH for real time data, no way

**CONCLUSION** = Big data requires different tech from RDBMS
Big data = Requires Big Performance

## The story of performance and availabilty
How can we make a database faster?

- Database Tuning
	Indexes and Mat View
- Relaxing ACID
	less isolation
	less durability
	fewer consistency checks
- Denormalization
	Fact & Dimension in DWH
- Scaling
	Vertical (scale-in): more memory/ram/cpu/...
		'Super Deluxe' hardware (RAID, Redundant RAM & Network Cards)
	Horizontal (scale-out): distribute over different server
		Clustering

### High availability
![[Pasted image 20220508225655.png]]
Usually in % 

Keep server online: with software updates, with failing hardware, ...

### Attack of the cluster
![[Pasted image 20220509230148.png]]
Node: is hardware on wich RDBMS is installed
Disk: I/O (usually HDD)
DataSet: Structure of the data stored on the disk

Clustering is the general term used when you have different servers - running on
separate OSes and also on separate hardware - working together.

#### Shared Storage
![[Pasted image 20220509230205.png]]
Multiple servers are available for processing, but that all data sets are stored centrally in one place (e.g. on a NAS, SAN).

Bottleneck: if shared storage fails, all fails, disk performance, ...

#### Local fail-over cluster
![[Pasted image 20220509230223.png]]
1 active node, and 1 or more passive nodes.

All changed done by active node, other on standby. Still shared storage (Bottleneck...)

#### Replication - Shared-Nothing
![[Pasted image 20220509230247.png]]
Each node full copy of data

log sharing mostly (actions logged and performed on other systems)

#### Master-slave scale-out
![[Pasted image 20220509230307.png]]
master = updates allowed
slaves = read-only

master= used for updating
slaves= read operations

node switching, when master fails, slave becomes master. (set up system yourself)

good for many select and few updates

#### Multiple master
![[Pasted image 20220509230326.png]]
Data is coppied between all servers
Always have a node that can take over.

Might be a time difference on updates between the servers, which leads to data inconsistency.

Big load on the network due to all the replication needed.

#### Sharding
![[Pasted image 20220509230411.png]]
data is distributed over several nodes (each node has access to a part of the data)

RAID 0

Benefit: several data sets can be queried at the same time

when a node fails, there is no availability to that data at that time.

#### Sharding & Replication
![[Pasted image 20220509230426.png]]
best of both worlds

Disadvantage: difficult setup

alternative: create subclusters

### Examples
sharding: distributing rows of a table between different servers.
Master-Slave Scale-out: MySQL uses bin-logging to keep all the servers consistent
Clustering: Oracle RAC (20k per processor)

## Conclusions
NoSQL nothing but benefits: 
- relaxing ACID
	sometimes necessary
- simple and better to set up in clusters
- dynamic schema
	difficult for etl/application/... connections
- more focussed on efficient use of data instead of efficient storage of data

! not holy grail, depending on context, good alternative.

## Polygot persistence
someone who speaks a lot of languages
![[Pasted image 20220508233149.png]]
![[Pasted image 20220508233318.png]]
+perfect tuning of the data store to the needs of the problem domain
-Complex infrastructure -> risk data consistency increased... and difficult ETL jobs 