.# Business Intelligence
## The Traditional Data Warehouse
### Back Room Application Architecture
![[Pasted image 20220508205402.png]]
Used in lot of companies still, who do not use large data volumes.

ERP => Offer a data warehouse solution, which makes the DWH itself
	Because it is hard to it yourself

### Important Source Systems

Operational Data Stores (ODS)
- Single place where all info is stored.
- "integrates operational systems into 1"

Master Data management system 
- Need grew from the moment companies knew they needed to do something about data governance
	- data governance = "managing your data in a good way"
- Maintain the master records of key concepts
	- With business info, you need reference information (student with course, staff with course, ...)

XML & JSON
- Receive much info in this format

Message queues
- dealing with data in real time,
- very complex
- loose coupling

Big Data Sources:
		=> No garanteed structure 
- complex structures
- Unstructured text
- Images and Videos
- Name-value pairs

You can also get stuff from external suppliers, this can be REST, ...

### Back Room Application Architecture
#### Extract
data profiling: Trying to understand what the data is about
- Structure
- Consistency
- Quality
- Usability

The phase in which data is read from the various source systems

=> Makes use of CDC (Change Data Capture)
- Only process modified data since the previous run ==> Efficiency
- Change data capture (CDC) is the process of recognising when data has been hanged in a source system so a downstream process or system can action that change. A common use case is to reflect the change in a different target system so that the data in the systems stay in sync
	- Methods:
		- Save creation and modification timestamps in source systems
			- 
		- Execute Diff
			- Compare The previous extract with the new version
				- Only modification is kept

#### Transform and Load
##### Cleaning and Conforming
###### Cleaning
Data quality is essential in analytical system
	Data in source != those quality requirements
	IMPORTANT to identify those quality problems

Levels of quality:
- Field level
	null where it should not be
- Structure level
	FK gets violated
- Company rule level
	customer has privileges, even though they do not meet the requirements

How to handle them in ETL:
- Correcting the data
- Interrupt the entire ETL process
- Do not further process the affected records and set them aside
- Tagging the affected records
###### Conforming
if you use multiple sources, align those:
- bring them to the same level
- Link them together
- Omit irrelevant data
- De-duplicate

##### Delivery
Delivery needs to be done after the previous steps

Often done in Star Schema

Issues that must be looked at:
- Handling SDC's
- Linking fact tables and dimensions 
- Handling data that arrives later

#### ETL Management Services
= Job scheduler and monitor, Backup/recovery, versioning, ...

#### ETL Data Stores
intermediate results are stored there in tables/dataFiles, all metadata is stored in this place.

Datastore, between the OLAP and OLTP

#### Metadata
= data, about data...

* process metadata
	* turnaround time ETL jobs
* Technical metadata
	* Source descriptions, job flow descriptions
* Business Metadata
	* Business rules used, data dictionary

#### Output
filled dimensional models, that can be used directly for analysis and reporting

## Evolving to newer architectures
newer architecture = different look = same principles

### Two tier with data lake
![[Pasted image 20220508215607.png]]
- contains all the data a company can use for analysis
- In its raw form
- Big data infrastructure allows for relatively inexpensive storage and processing of a lot of data
- Source for the data warehouse
- Used as a sandbox to explore the data and its value

**PROBLEM** = Data Quality

### Lakehouse
![[Pasted image 20220510220955.png]]
Combines DWH with Data Lakes

- SQL like functionality
- Extra Performance
- Add metadata layer
- data lake --> ACID by adding transaction
- Time Travel: consult the history of data objects
- Access control
- Access logging
- Data Quality:
	- Schema enforcement
	- Constraints API

### Data Mesh
![[Pasted image 20220508220239.png]]
Centralising Analytical data in a single system

Problems:
- failing ETL-Jobs with the encreasingly difficult complexity of data pipelines

Bounded Context == Central pattern in DD
![[Pasted image 20220508220346.png]]
- Uses own operational data to create an analytical data product 