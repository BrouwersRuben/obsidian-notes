cd # NoSQL Databases
* non-relational in structure
* often schemaless
* easy to cluster


very few players still 

## Common types
1. Key-value stores
   Entity linked to key
	   - No structure
		   - Blob (images, serialized files, ...)
	   - no aggregation, just findOnKey
   Looks a bit like a Map in java
   Used a lot in web apps (shopping cart history amazon (acid does not really matter here))
   **Riak**
2. Column family stores
   Like key value, but values are a list
   +Good at writing out lot of info
   -No filtering for fields other than indexes, also no joining
   
   Use when most entities have similair structure, if any relationships use document db
   
   not well suited for analytical stuff
   
   **Cassandra** (netflix)
3. Graph databases
   emhasis on relationship between entities (relationships are also being given labels)
   
   Social media uses this a lot (who you might now)
   
   Node: facts, actual content or value you want to obtain
   Relationship: link between nodes, may contain attributes, can be uni or bidirectional, used for filters in queries
   
   Dates are often modeled by components in nodes
   
   - You have entities with many relationships
   - you need real-time insight into the data
   - relationships are very important to keep track of and to look at.
   - you want to analyze the relationships, not really the data.
   - example:
	   - social computing, business intelligence, identity management, ...
	   
   **Neo4J**
4. Document Stores
   track data (json) in documents, and bundle them in collections.
   each document has its own structure, only hierarchical, containing data that logically belongs together.
   
   not normalized
   
   Can store more than 1 entity in a document --> aggregate, or aggregate orientation
	   in NoSQL = not how you query it, but how you structure it 
	   ![[Pasted image 20220509001956.png]]
	   You can also make a reference to an other object, similar to FK, but you should avoid this as much as possible
	   **Advantages**: performance rises if data is often written and retireved together (fewer joins), a document is good for sharding and replication, highly connected entities can easily be mapped together.
	   **Cons**: denormalization (SCD2...), (too) strong coupling between app domain model and db struc 
   
   can look up data efficiently, but requires different mindset.
   
   Advantages: 
   loads of entities with parent child relationships, and thus can be treated together
   not all instances of an entity have thesame properties
   
   **MongoDB**
5. Seach engines
   Data structure to look up information
   format similair to document store
   
   Each 'full text searchable field' gets an inverted index
	   normalization rules:
		   - Mood (verb to infinitive, plural to singular, ...)
		   - lowercase
		   - synonyms (leap --> jump)
		   - ...
		for each normalized token, a record is kept of which document appears.
		How to tokenize and normalize is up to us, check the analyzer on how to do this.
		Analyzer is fully configureable
		a "full text search" returns a result with matching score that indicates how relevant the document is according to the search.
	
	When to use:
	- make data easily searchable
	- search on relevance instead of exact values
	- document store use case
	  **Elastic**


## MongoDB
Most popular **Document store**

Database:
MongoDB server can contain multiple databases

Collection: 
A database consist of one or more collections, a collection is a list of documents.

Document: 
hierarchical structure, stored in BSON (binary json)

### Introduction
* Schemaless
* Documents do not all have the same structure
* Hierarchical structure
* Transactions
	* Atomicity per document

### Sharding
Distribute mongoDB collections to different nodes (=different servers)
![[Pasted image 20220509010726.png]]

components to do sharding:
* Mongod instances run on each node where a shard needs to be stored (several servers)
	* they do not talk with each other, just the router
* Mongos instance is responsible for routing reads, insert and updates, this will be addressed by the client , giving a single point of access.
	* stateless, run on app server
* Config: keeps track of where exactly what data is stored in such way that mongos know exactly where it is.

No SQL possible

#### Shard Key
shard key = functional value for the document

determines which shard it should go into.

1. High cardinality
   ensure optimal distribution of shards by providing a lot of variation in the values of the shard key
   Not just gender, cause that only gives 2 possibilities.
2. Write Scaling
   successive inserts are best not done in the same shard. In this way, insert and update operations can be distributed as optimally as possible and thus increase performance because the processing time is spread over the different nodes.
3. Query Isolation
   choose the shard key in such a way that data that often needs to be fetched together

These 3 characteristics will work against each other.

Alternative ways to calculate a shard key:
- Composite
- calculate ideal shard key and add to each document
#### Chunk migration
![[Pasted image 20220509011112.png]]
MongoDB distributes chunks evenly among the different shards, if imbalance, it will do some shuffling to keep everything in balance.

Config server plays an important role here, keep track of the chunks distributed over shards.

when a chunk is too large, will be split into smaller chunks and moved to another shard if neccessary 

#### Replicate sets
![[Pasted image 20220509012901.png]]
a group of mongod instances that manage the same data set (shard).

Also use a master, which sends logs to keep data consistent

happens asynchronously, so no guarantee on consistency on the second node.

all nodes in a replica set ping each other to see if the others are still up, and if they have to take over. this choice is made with some kind of election.

Arbiters can be added to a replica set, to make these elections go smoother.
![[Pasted image 20220509012926.png]]

rules for election (performance, response time, ...) <- out of scope

Since 3.4 = mandatory to have replica set for each shard

![[Pasted image 20220509013038.png]]
==> Solution is high performance and high availability