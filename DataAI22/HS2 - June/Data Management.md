# Data Management
Data Governance is a system of decision rights and accountabilities for information-related processes, **executed according to agreed-upon models** which describe who can take what actions with what information, and when, under what circumstances, using what methods.
- Who is responsible for and can make decisions about data linked to processes
- Established models are used consisting of a collection of rules
- That describe who can take what actions with what data, when and under what circumstances, using what methods.

## Goals
- Increasing the consistency and trust in decision making
- Reduce the rist of fines and around regulations
	- GDPR
- Increace data security
- Increase revenue from available data
- Increasing efficiency
	- Data cleaning will requite less energy
	- Different services perform the same work

## Universal Principles
- Responsibility
	Who is responsible
- Transparency
	must be clear to everyone how, when and why data related decisions are introduced into the processes
- Auditability
	every modification must be tracked by who
- Stewardship
	Data stewards are tasked with checking that data-related tasks are performed with respect to the established governance rules. 
	Stewards typically operate from an overarching (and neutral) department that is closer to management.

## Key Activities
### Metadata management
- Data dictionary
	- Describe the terminology used within the company
	- prevents mixups
- Mapping data flows
	- what data, and how was it transformed for a specific DWH

### Data Quality Management
Examination of the current data (with goal on improving)

### Privacy and security management
Define how data will be secured and how data loss should be avoided.

**GDPR** (General Data Protection Regulation): May 25 2018:
- **Transparency**: the person whose data is being processed is aware of this, has given permission and knows his/her rights.  
• **Purpose limitation**: personal data is collected for a specified legitimate purpose, and may not be used for any other purpose  
• **Data limitation**: only the necessary data required for the intended purpose should be collected  
• **Accuracy**: personal data must be and remain accurate  
• **Retention** limitation: personal data should not be kept longer than necessary for the intended purpose  
• **Integrity and confidentiality**: personal data must be protected from unauthorized  access, loss or destruction  
• **Accountability**: the responsible party must be able to demonstrate compliance with these rules

### Information Lifecycle Management 
Rules defining who is responsible for the collection, use, retention and disposal of data.

### Master data Management
Managing central data in the company.

* Within DGS (Data Governance System)
* Master data = Reference data
	Key entities within the business that are referred to from processes
* Process data IS NOT master data
	Processes are typically linked to only one operational system. So there is no point in managing them centrally.

![[Pasted image 20220611230442.png]]
#### Why
![[Pasted image 20220611225219.png]]
#### Defenition
The methodology for making key reference data within an enterprise available in a single place.

#### System of record vs. reference
- System of record: 
	MDM is manager of master data
	data is stored in MDM
	system that uses certain data must link to MDM
- System of reference 
	Source systems remain custodians of master data
	MDM collects references or copies from the various source systems
	Systems can get a full picture of master data through the MDM

#### Linking Resources
If there are more than 1 source system in the MDM, then the MDM must links them
- System of record: One-time, at the time of release  
- System of reference: Continuous

##### How to link
1. Combination of rows: 1 --> 5
2. Estimated percentage that it is about the same person: 0%
3. Reason for that percentage: No column equall

![[Pasted image 20220611232050.png]]

Often use automatic linking:
1. User set some rules
2. User sets matching score threshold
3. If manual intervention is necessary, the data steward will have to  
determine if it is about two equal rows

### MDM Implementations
![[Pasted image 20220611233041.png]]
* Data from different systems becomes :
	* Transformed
	* Cleansed
	* Merged

#### Consolidation
![[Pasted image 20220611232930.png]]
- Reads from the source system, but does not modify them
- System of Reference (**does not manage the master data itself**)

#### Coexistence 
![[Pasted image 20220611232947.png]]
- Similair to consolidation, but the source system can also use the data
- Synchronization with source systems must take into account possible conflict handling
- the MDM is not always fully up to date (unless updated via streaming)
- System of reference

#### Registry
![[Pasted image 20220611233003.png]]
- Data from different systems becomes:
	- Linked
- The MDM system only maintains references to the source system for each MDM record
- A query on the MDM causes the MDM system to look up the various relevant data from the various systems
- System of reference

#### Transaction hub
![[Pasted image 20220611233017.png]]
- The master data is central to the MDM system
- This is single version of truth
- The source system themselves have no masyer data
- System of record

### Building an MDM
- not a BI task, but data govenance task
- Ensures better data quality in the operational system AND BI envirroment
- Reduces effort in the BI envirroment (Cleaning/Transforming)
- Exists as a source for the BI envirroment **alongside other sources: can only provide reference data**

![[Pasted image 20220611233304.png]]