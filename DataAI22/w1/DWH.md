# Introduction into DWH
Operational data = Loads of valuable analytical data
	- Problem 1: retrieving data demands loads of resources 
	- Problem 2: data is spread over different systems
	- Problem 3: Technical knowledge of the systems is required

**SOLUTION** ==> DWH (Data Warehouse system)

**Defenition**: A data warehouse is the only queryable source of data in the enterprise. It’s based on a frequently updated copy of transactional data, and it’s specifically structured for query and analysis.
			- Subject oriented
				- The organization of your data happens according to subjects around which analysis questions can be asked.
			- Integrated
				- internal and external sources can be used
			- Time-Dependant
				-  You also process in your datawarehouse historical data from your company

## [[Why use this]]

## [[Different types of analysis]]

## [[Why seperate IT Component]]

# Design
* [[Star Schema]]
* [[ETL Design Considerations]]
