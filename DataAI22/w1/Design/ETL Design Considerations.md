# ETL Design Considerations

**ETL = EXTRACT, TRANSFORM, LOAD**

ALWAYS USE ETL TOOL

Why? => puts performace last on the system that is doing the analysis.

Otherwise you do it on the systems that are not designed to do that which will lead to performance issues

And this way you are not dependant on the source DB as far as performance is concerned. Perform simple queries on the source DB and the ETL tool will transform this data. 

This way you have the **optimazation** under control.