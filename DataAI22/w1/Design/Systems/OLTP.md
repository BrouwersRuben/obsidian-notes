# OLTP
## Normalized
- Focus:
	- Efficient data storage
		- "A well-designed operational system is fully normalized. This ensures that no duplicate data is stored and that storage is used as efficiently as possible."
	- Frequent updates
		- "Throughout the day, small changes are made continuously (e.g. New orders, New customer, Update customer...)"
	- Simple queries
		- "If a select-query is executed, it is usually very simple with just a few joins"
- Many tables
- Many relations