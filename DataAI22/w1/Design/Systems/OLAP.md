# OLAP
## Denormalized 
- Focus:
	- Efficient queries
		- "We often have to process a lot of data for our analyses. Therefore, we need to make sure that we can write efficient queries. We give up efficiency storage for this purpose."
	- More complex reading operations
		- "The system must be able to handle a lot of read operations"
	- Limited number of updates
		- "Updates are relatively less common than in operational systems. Often, the OLAP system is not kept in sync with the other systems in real time."
- Few relations due to denormalization
	- To do this efficiently, we need to denormalize the data, so that there are never any complex joins
