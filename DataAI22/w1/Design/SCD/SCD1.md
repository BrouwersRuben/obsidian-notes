# SCD1
change the existing record when there is a change in the source system.

## Downside
History will not exist, for example:
if there is a person who changes offices, and you want to get all the sales from that persons which he made in his previous office, that just won't be there anymore.