# Dimension Table
Goal = categorize and filter facts (how many product have I sold with cat x)

Contains:
	- Surrogate_key
		- Why? -> Mostly performance
			- natural = composed of serveral attributes 
				- When you join = link to other attributes => slow
					- Fact table also has to store additional columns then
	- Natural key
		- product dimension = product_ID
		- Will be referenced reports
	- Dimension attributes
			-  all kinds of attributes that can be used on a report => content readable
				- because the attributes are directly used in the report
				- under **NO** circumstances add additional tables for different catagory values.
				- This will cause the performance to decrease due to the number of joins
			-  Include as many usefull categories as possible
		- Dimensions should have little rows (respect to the number of fact rows) and many columns
			- No limit in the number of attributes
				-  Additional attributes can be usefull for reporting 
			- limit yourself to necessary number of rows
				- because you always use dimensions to categorize and filter
				- Database is going to have to go through the entire dimension
					- fewer rows = faster query

Many fields often have the same value => result of denormalization => inefficient storage (does give faster queries)

report query /= complex on (on dat warehouse)
	- The necessary joins between the fact and dimension are made to only retrieve the select data needed in the report

## Slowly changing dimension
contents of dimensions change over time

3 types:
	- [[SCD1]]
	- [[SCD2]]
	- [[SCD3]]

### What do I use
depends on the analysis questions you want to ask.

SDC2 - quickly increase the number of rows (only use this if absolutly necessary)

### Are there other types
type 6 (2 * 3) = combination of 2 and 3

[wikipedia](https://en.wikipedia.org/wiki/Slowly_changing_dimension)