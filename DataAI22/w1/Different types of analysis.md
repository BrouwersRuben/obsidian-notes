**![Information Value Chain](https://lh5.googleusercontent.com/p95QXheJ0_dX4AKmLHL3L6K2c-vYrr9lgc9IdoAUsePHZxNsbXP3Zz6HQtUI1DIAdeSe9byWPl1lz-uzZ92IsdWUo-P7OhySScUGylhj3-kKbieY6XGB3xtM3lJA3vrPKW8uXf5Plbll)**
# Descriptive
Making simple reports

## *Example*: 
Sales per day

# Diagnostic
Investigating which factors influence certain figures

## *Example*: 
Why are sales of consoles so much higher in November and December?

# Predictive
Predicting what those numbers will look like in the future

## *Example*: 
How many consoles do I need to produce to have enough stock in November and December?

# Perscriptive
System looks at possible alternatives and prescribes the best alternative

## *Example*:  
Amazon's analytics tools look throughout the day to see if it needs to adjust prices. If a competitor adjusts its price, Amazon will automatically see if this makes it wise to adjust its price. Amazon knows perfectly well at what price difference for such a product the customer will switch to an alternative supplier. There are 2.5 million price changes that are made daily at Amazon
