# Different types of systems
### [OLTP](OLTP.md) - System Online Transaction Processing 
- Day-to-day company processes
	- invoicing, inventory managment, etc
	- Optimized to perform a large number of small updates and highly targeted operational queries
		- "Many updates, simple queries"


### [OLAP](OLAP.md) - System Online Analytical Processing
* Analysis of operational data
	* designed to perform analysis
		* optimized for complex queries
			* In the past, the updates were limited by the limitation of earlier systems, recent years that has faded -> Big Data Infrastructure
			* So ==> characteristics of 'limited number of updates' is fading
	* Focus = optimized for analytics
		* "Limited number of updates/Complex queries"
