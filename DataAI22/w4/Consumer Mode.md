### Ad hoc
"to this", a solution for a specific problem
		- a priori => more generalized solution
	- use the analytical data to create an ad-hoc report, graph or data mining model
### Push-button
retrieve the necessary analytical data with the push of a button
### Operational Reporting
enrich your operational systems with analytical data.
