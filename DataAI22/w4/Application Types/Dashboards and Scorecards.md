# Dashboards and Scorecards
* Consolidation of information
* Alarm Signals
* Aimed at specific User Profiles
	* e.g. Helpdesk Staff, Sales Management
	
## Dashboard
**![C:\Users\overvelj\Dropbox\KDG\BI\dashboardCallCenter.PNG](https://lh5.googleusercontent.com/VKi-_QW6bJkoEsehz3IQ5cP9U9if5iMMB-OwkEN12Ug52MphSxuJG4Trfeqou7KqPCUhDJwhBJvAhJq46lqGIOl1k_InMpb5gkMt283teU8_67ECMV4eMRtrvzOhtrcsCOzzP1Hk1fDy)**
## Scorecards
**![http://otusanalytics.com/wp/wp-content/uploads/2012/10/Screen-Shot-2012-10-29-at-11.23.26-AM.png](https://lh4.googleusercontent.com/Gthl2fkiMdDuMEzv-PvmcDdgkAa8m1_nSjPygYQWNzlD4m79nmRVjuth8RBG6GleLIbopm4vgydFUwb7stxTj7h7gLbdsXOX993Jp3wBh1TI3UdRHhWVz5VC7rxppA3o7lpE9KR0kp6f)**
