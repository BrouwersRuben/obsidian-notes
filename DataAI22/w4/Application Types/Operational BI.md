# Operational BI
- supplements an operational system with broader insights
- this data usually comes from the BI/BA environment
- often based on more complex analytical models

![[Pasted image 20220309094150.png]]