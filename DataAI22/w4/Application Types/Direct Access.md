# Direct Access
users perform analysis themselves

## Query and reporting tools	
**![](https://lh3.googleusercontent.com/Q0FBFNUFMUEmQkzicro6-oHx5uRe75C-1JJhhK8pW5Cbv9YeALl4j9BoDo6EkgwNJ0MxMUG1cm6b2ApXegaPqmHPehd3Lr_IkrZ4ZWKS0JAFFcYLLnYtt7IM8FuVz2O29ywgeYCoCujP)**
reporting envirroment = accessible way of creating reports
		- have average degree of difficulty for business users (=>Training required)

You can use drag & drop to place dimensions and measurement values (see star schema) on graphs and reports

DI and reporting is usually in the same envirroment 
have semi-direct access to the data models via the **metadata layer**: 
	- NO DIRECT ACCESS TO DB
	- but metadata layer (mapping of the relevant tables onto a readable structure)
	- defines:
		* Which tables are available in the reporting environment
		* How the tables are linked to each other
		* Who is allowed to see which data: Role Management
		* Which names the fields should get with multi-language support: 
		* technical names -> reportable names
		* consolidates tables to be included and the way they are linked together


Reporting Environment:
- analysis and presentation functionality
	* functions on the read data:
		* pivot
		* drill down
		* simple calculations
		* sorting
	* complex formatting
	* charts
	* composite documents
	* customisable paramters
* user experience functionalities:
	* user friendliness
	* drop-down lists
	* integration into other applications
	* exports
* technical responsibilties
	* multitasking
	* cancelling queries
		* "if you see something that is not going to work, you should be able to cancel it"
	* scheduling
	* security- 

## Data Mining
**![Afbeeldingsresultaat voor rapidminer](https://lh6.googleusercontent.com/3-Hi-0qdJKSwvNgUe_h4vyzhXVcCcMZag13NaYUSjKSu_zobXWeXN4RNYZdC5K-sma4VRBujKt45YhNLeC5MbHAn0VNijJRUT45WSrcMksnd1Jlqg0Zvf-ZeEWCGtFtt2gEYH9Svmf0f)**
Ad hoc more complex analysis models
task of the data scientist
	- specific profile in the company

you use the data to build analytical models. This is where clustering is applied.
	- Not every data mining tool is graphical.