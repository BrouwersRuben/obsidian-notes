# Standard Reporting
- predefined formatting
- Often with paramter options
	- "Filtering on ordering status"
- very simple for majority of non-technical business users
- immediate added value because they provide direct access to data from the [[DWH]] 
- official source of KPI's
	- Key Performance Indicators

**![http://www.netrocart.com/Media/Images/Features/E-Commerce%20Sales%20Reports/daily-sales-e-commerce-report.jpg](https://lh6.googleusercontent.com/w_sm7c7Tq8AhVpQgysFKPg1b1W7rdTflUB4k5BJf_rmHihJUxRtCqWrFRRKHEcXvNts4SsveB8Nd8zAH9Hp4xtJEf0uqHdjtb8TLfaCHZQ50YA8IPzgEREuN31z0nvhKQ0lALkucYU5r)**
