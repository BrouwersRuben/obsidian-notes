# Analytical Applications
- 1 specific business problem
- Off the shelf packagen or Saas (Software as a Service)
- Apply complex machine learning algorithms
- Can be plugged into business process

**Benefit**: no limit for in-house development and know-how
**Disadvantages**: risk that these products funtion as islands (not integrated/Secluded) within the BI architecture

## Examples
- shelf planning softare
- web analytics
- fraude detections
	- sift
- targeted ads