# Analysis Type
- **strategy** = company goals
	- "Retain the top 10% of employees in the company"
- **tactics** = specific tasks to reach those goals
	- "Provide a high competitive compensation package and give them the possibility to take a sabbatical."
