# Data Visualisation
"*You want people to look at your visualisation and understand it as quickly as possible.*"

## Gestalt Principles
>"Unified whole"

1. **Proximity**
	1. We tend to think of physically close objects as part of a group
	2. **![](https://lh5.googleusercontent.com/cSandMlz3wcFN-Qr14B7JARVCx-c6xguiHNyLtMNTscNuzp1K-ub6HcJzatEc927patKFE1gzBSADiOtus1k5UR2y-3x-R6-IoWfSqDkcLsiNU5VOlEqdLOY4zv8NpHUtnC0hEgZBRLP)**
	3. **![](https://lh3.googleusercontent.com/UlXx0ShtI3T1a5QyUEWlDGgX-Z0rIKV2QNtVbHlws5csIwvSQ9hHEySdgCGWZrdBF6_5PRMUpBBOFcG7izrEC7qga-vOAcrTHKF9Sxo1rVoyvhDd38lAPw00fc16zXl1M3PXhkCWgZGj)**
2. **Enclosure**
	3. **![](https://lh5.googleusercontent.com/reS95tMDeMcip69lKwEk5pf1IMt60Jrzx1QiDDNUfiqqRy0cuyyCUqiiDjA6PD7SIbCq10zKQnnlYt23z-IDeq8LrsFF0gBTdUvRL5zx8R9-EXcJLEqptw8mDzb16pmHLaPOlwcWTMAd)**
		1. The 3 groups of dots are not that clear anymore, we do see the enclosure
	4. **![](https://lh3.googleusercontent.com/s6pOinoJv8QkPNwvjfDv4Wo9YzqAxIy8smEVYK9UGaKl0K8nogSb5QmL18y4JsYcYQThZeREMLjUaT2NcFy8-C0yCw3s8BLeQQNN70aMo2duHef979H8OzEw1pvWQv2dM8He8Zqodlfp)**
3. **Similarity**
	1. **![](https://lh4.googleusercontent.com/Op909j6GX8LNJ2XH-FUhMn2pYL9sZxMDXclqzXoFV2wkOCmGofaTGQvw-vMQZFeUywCGcon1pJoK8RwVXlPIhd4M5xQlvCkfVTVMqMTpda0CiwrBSDuWU-Vk5CisWXHvL3dPHypzJnwM)**
	2. **![](https://lh3.googleusercontent.com/BjKf7TkLoGTM5rATjJDG1Haf1ryLkfLJqma9kSOLaqhAwf6J7YndXJxe1EUWSPYOetk9jAV5vHVWg-Nstu4S6DV3kWSTgN3iO_dUksWN5SXlTtRLzYxcK3TnKkU6CBBq2xYXlkECMN9o)**
4. **Closure/Continuity**
	1.  ![[Pasted image 20220330234749.png]]
	2. **![](https://lh6.googleusercontent.com/IjTXjOYRsgp_MffW0JmXHZOIO_Z392HIHYziD9VB3XSbvFK0JqXvbe1IHyAVXMTbnzY6bp2ns0aBwS3rdZv_3tmMSIoDGtnVf9ovp2GE9Ih0L6jmpOZlUSaCWvnuJB-o4_XlU4bbC5WS)**
	3.  Closure: Your brain sees the dotted lines as a circle because your brain automatically connects the interruptions.
	4. Continuity: Your brain looks at the simplest path 
5. **Connection** ← Very Strong
	1. Connection helps grouping in very strong ways.
	2. **![](https://lh5.googleusercontent.com/c5O46IFZhZgjegB_EKHsowTktye-5pj04r6v-A7nxYR3h-uDcjIc51WltPewtJmXaDFCph67j2jj9Pa3EZJT3Q7rL16G_VuFOZdGCGA43smFnBgtnVu2GGzeE2zOyxQQjbW8LqON4tCD)**
	3. **![](https://lh3.googleusercontent.com/ijorF4OAAcjnYHsdTvEl3fmCKwh9cwR6RQ-pq_Fc2WbHJPuyzW4bwXYSbS96dHx84F4SlENFlSLQSwSnUDojlqAWtQjeAT6qdxzA_NleK4rmfWMU80ljQvOTtvYPX3xe3hVwB_Zmpwqm)**
---
## Declutter
>*"Get of the grid what is not needed"*

**![](https://lh4.googleusercontent.com/mLvt4nUlJxAdDUUe--Ie-Z0Pgh2YyrS8EtTt1H5DupxYN0PDAFbrb2eitgcJQGemW17QWhYetOBjofHEX20b2ntzestJ80-Ib7tJT4EeBAFhTq7l2rvh57GaNtDLzWl1IxAWQ7lVQWoD)****![](https://lh4.googleusercontent.com/_m6oALMVt4Z_pNSuRb3I-P9HvLnASMWwyWTWUXzPuWoONYNBTL9wSeKZPck0MOqXlRk_Z0CzemDzE0ksT2pU44iMFqUtoxBX88cLSviX_ZDev3cjPp2DpfH6AStdy9ftRT95BZtzRZar)

---
## Draw Attention
**![](https://lh6.googleusercontent.com/gIdh78t1rYaCHcvwsQ2ocV00QVTNFZL3hJGsUN9LI50KYgX_-JPOIfP_P3mrVfLF9Vbnpu7R4jxKdZ-QPAAJHRS_fZeEDXGwn4yHgQPQ3FoBDnfrd_gQFMoiFxCgrSpZgh7Rv1As5mzK)**
### Example
**![](https://lh4.googleusercontent.com/_m6oALMVt4Z_pNSuRb3I-P9HvLnASMWwyWTWUXzPuWoONYNBTL9wSeKZPck0MOqXlRk_Z0CzemDzE0ksT2pU44iMFqUtoxBX88cLSviX_ZDev3cjPp2DpfH6AStdy9ftRT95BZtzRZar)**
**![](https://lh4.googleusercontent.com/4WvKrvHsEwoER2q6sJbuZgN186j7yHId9AW4hHQCmB0LZDN1MrlA_S1bhom4yNgwWY8iJM9TOoXoO2rnAfPRXAe_N6HTY6xI_XGVUtX7uB5VoBJiO7jfcPE5OWCK5AjsUrAj0ufURCbs)**

# [[Examples]]
