- **Fairly cluttered**
- **Bad alignment**
- **Draw attention to what is important**

**![](https://lh4.googleusercontent.com/RcUV4cxkgXlTFOHBFZ8141jy4XT33MOZ71HONuTtsvK4F9ksfRUjUOISzBOARBre0lY8jTCO76PQuGWggsyZzpSgkctBhJjj1dwEl6wU-tgDYi-uKiPrQVGoCKmWVl1gznuYIqFFlmZG)

****![](https://lh6.googleusercontent.com/WMsQxzkqvW9bTBj9TxyXjcj_TJXeIc2YxnczW1y5rLBTnXtFRSJ4X5qJ16tEaFO49dNsGhNtKHz6vBR8yRX94EI9p6xBFkJx2TQjNrVa5vjuFXgKBRbMkWq9fBOMd-G_8uUfELHDIZOG)**

---
---
- **Very difficult to interpret**
- **Cluttered**
- **Comparison is almost impossible**

**![](https://lh5.googleusercontent.com/2ibj8vZwEW8Topr2QKZ1MFZ_5dpWOc5pZhwnbeTr8dk6LvRGeeMxJU0vhAccQi0WaKmJ8Q3qm421w3SnVtnVFqeccu3SaQDSl_wTQnRUOLKXyBptXwCdlrYlbgntKly_ifnW3WQSEbdL)**

**![](https://lh5.googleusercontent.com/fNRtQ4Ow9SAsyOVdM8P6T04V-sOd5pIU4pzpD1Y_pRcPg7E3cWGYQyOu5hp0O7ZXgMKweumHSLvxHhI46Dfwr0uiK8recPtYHB9UyBbOZdIurXm8WLJWwL-LfNfHPfl5O2GX_6K18bAW)**

**The indices have been removed but that is justified here
Ranking is added, with the emphasis on our business
The bars start at -1.5. The zero is removed.**

---
---
**![](https://lh6.googleusercontent.com/8DwRi0bzBHUgJwWabSmsX1DY7JdcEG85JVKTQAStl0C4utPBjt9QilAGX3EXbd1hncdTj9JbPX_3-7yMF5Qy0LGswFGls7BqglNOG-YRhi2TjFgM9TfM5X5FVeZPh5IeGHk5_JBazqyC)**

**![](https://lh5.googleusercontent.com/5TbEd7L_zvDJOA4UaQ3i1QfN6F4vslCdOaObfK_o_6o3qeJuVcdmzzO2wdVogVNyaQxgFXON3yEvUOec7YcnYtDwhrn8uYK3Pf5u_9RasfkKMg836u1gh09VCtITOaHoUXvE1Cz0FvOi)**

---
---
**Bad choice of color scheme**.

**![](https://lh5.googleusercontent.com/p8rrF-HmXfFyK1cI52v_BjismgV9N-Pq4Ju-XbXkDpujlc1mgHei_d78mf_wy_YRpL-_M7KikqNJVzLnzu3kw4wCr8fBOuAkPPAiKR_GlvGc0zS-6oLnsSk46WvvSE18CSXOmkoa5y6p)**

**![](https://lh6.googleusercontent.com/bF-muDMrHDK_baXsM-0GrP_QGcQMnte_S5ICH7N-SRv4MPkv6qEmixK22ROYv2Xi7hBqEiCqP414wMtap0Q_PpDUHDbreAZ4dK7aza7RPRi2jgQHxDO3bd6wi1cOCrefo7F7PQlX8_xi)**
Use accent colors, use less categories and emphesize.

---
---
**Overall declining trend in Marriage rate.
The Marriage rate rises with the level of education**

**![](https://lh6.googleusercontent.com/dI9qZZ0KP7YwthJuVHZkR-tXiF_yT-S1LSyT9bPIS47cYVZ9QMGgeon6iEJliCqDfAyIujYCXuvqhReNEyKHHb0yI8UBpXM8MafSCjP5O9PRBFWYjTrsm9MKRTNDXNBS3wieQMQ6fWm5)**

**![](https://lh4.googleusercontent.com/8TyclFqv6b2j5moL62U_pJEAUghTZKOVYmS3LMnYmdriPHAiJk6lIlcPk0VCj-CX2Dd77O2NPkiyMFm6LcFIlmol52Nwh84-wSAtYRlLfvIRJffKi55Vkk6_HnYpUpojzGiEqAtJM7Ij)**

**![](https://lh3.googleusercontent.com/OVu6jGGvVf16f5dDEKZDd3xZzx1xBnFuPPxI2fTvLYBvKk96TN-GD7yrS3Ruj1TlHqtLUNa4iYk2CS32q9OY1a9GObmXYpecRdUpR8FncXlgUkunNby29iJkePowXAY_kYvmIbFaAn6-)**

---
---
**The color scheme
Cluttered
The box should create emphasis but disappears in the clutter**

**![](https://lh5.googleusercontent.com/-v7Gx66q62FjlqJZLNSsFoy1zWqixaD9i6wdddhyhFG3Q_hnxu6TGN3ME5htyGJNU3uAP3QIQllEoyvI-RF8gm7klp-gW0YfIXmCaW17pw8LsMQTsL3BRGmtvg6aIOZ9Nxd0UJgdJZpW)**

**![](https://lh6.googleusercontent.com/OWBLsJefM5u1NJxOyrEoslYO8hvnOiHvQIkNJ8EEGEU0lyvEJw84ccccJA50tiMntEXQRxWjhdusGIJnMPuodd1u97c9I1KrbhboLmS1h251gjsYHLUZFngFg-zEOE3sgpqMutiCbTC5)**

---
---
- **What part is the biggest peace**
- **Never use 3 dimensions**
- **Pies are almost always a bad idea. Only use a pie chart when the categories are limited and add percentages**.


**![](https://lh5.googleusercontent.com/YM1wyf8Gb2rVkkCsESA2ydkjwsEVSYksNuviebHlWpR9ayw4UQHoRkuDeHYKAWSgsJCbFuxYYAjvt_GfO6kOm_yYYZBS5jraJ07j_VlrJqJjbRN15QdRW8s8nIyoOEbvz9YO9lDpsJYi)**

**A bar chart can also be used to illustrate the percentages.**

**![](https://lh4.googleusercontent.com/SYI1tG49nYConQ8cmayFGdg9TziTeD1uT5cLH91JpfUDUKmLTcRaaU0P70ZYe4mEbCV2TO5EhutDlUozCa1DjHkvXN6Lv0sk3xX0Qxdv93JiISujYywcz3T9JsIYQAm0jCJv8_QIjp-6)**