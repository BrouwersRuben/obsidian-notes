# Goal
The purpose of a [[DWH]] is to facilitate BI (Business Intelligence), to make good decisions managment has to have **reliable** information...

## Benefits
- Competitive advantage
	- Better insight into business than other competitors
- Increase ROI
	- Optimisation of company processes
	- Costumer retention
		- Losing a costumer and gaining a new one is 4 times as expensive as just keeping the current costumer
- Increase the productivity of decision-makers
	- Through analysis a better and quicker understanding as the basis of decision making.
	- ...

## [[Different types of analysis]]
![[Pasted image 20220216084514.png]]