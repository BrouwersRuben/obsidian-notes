# Analytic Cycle for BI
![[Pasted image 20220216091553.png]]
- Managment mainly only asks for the first step. In reality the entire circle is needed

## Steps
### Following up on Activity
Read through the standard reports in order to compare the results.

*Ex*: Report with the sales number per month for a console. 

### Identifying Exceptions
Study the outliers and the exceptions.
(Although the managers asks for reports, they actually just want to see the exceptions marked on that report.)

*Ex*: When is there a clear deviation in the sales off a gaming console?

### Establishing Casual Links
Investigate what causes the exceptions.

*Ex*: In addition to seasonal effects, what other factors play a role in the sales numbers of a gaming console.
 
### Modeling Alternatives
the relationships found can be used to examine what effect changes in business practices would have.

*Ex*: With a 10% decrease in console prices, I realize a 5% more profit (due to more sales).

### Taking Actions and Following up on Results 
Based on the alternatives, action can be taken. The results of these actions are included in the next analytic cycle.

*Ex*: The effective profit increase was only 3% in stead of 5%.

## Conclusion
In the next cycle we take the result on the changes into account and look for additional factors that determine profit.

A good BI environment supports this cyclical movement and encourages users to cycle all the way through it.